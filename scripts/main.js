"use strict"


let n;

const validateNumber = number => (number === null || number === '' || isNaN(number))

const getNumber = numberFactorial => {
    do {
       n = prompt("Enter any number for calculate factorial: n!", n ? n : '');  
    } while (validateNumber(n));
    return +n;
}

getNumber()

const calcFactorial = (number) => {
return number != 1 ? number * calcFactorial(number - 1) : 1
}

// обчислення без рекурсії:

// const calcFactorial = (number) => {
//     let factorial = 1;
//  for (let i = number; i >= 1; i-- ) {
//     factorial = factorial * i;
//  } return factorial;

// };



alert(calcFactorial(n));